package Players;

public enum Move {
    SCISSORS,
    ROCK,
    PAPER;


    //Här har jag en metod som bestämmer vad vinner över vilket
    public boolean beats(Move another) {
        return switch (this) {
            case ROCK -> another == SCISSORS;
            case PAPER -> another == ROCK;
            case SCISSORS -> another == PAPER;
        };

    }

}
