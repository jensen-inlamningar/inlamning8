package Players;

import Players.Move;

public class Player {


    //Här har jag gjort mallen för vad en spelare ska ha
    public Player(String name, int wins) {
        this.name = name;
        this.wins = wins;
    }

    private String name;
    public int wins;


    public String getName() {
        return name;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }




}
