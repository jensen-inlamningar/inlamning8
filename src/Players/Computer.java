package Players;

import java.util.Random;

public class Computer extends Player {


    //konstruktor som matchar parentklassen
    public Computer(String name, int wins) {
        super(name, wins);
    }

    //Datorns sätt att bestämma vilket valet blir, den randomiserar helt enkelt
    //vilket val det blir.
    public Move tactic() {
        Random random = new Random();
        int move = random.nextInt(3) + 1;

        return switch (move) {
            case 1 -> Move.PAPER;
            case 2 -> Move.ROCK;
            default -> Move.SCISSORS;

        };

    }
    //Den här metoden adderar till vinsterna som spelarna har
    public void incrementWins() {
        wins = wins + 1;
    }

}
