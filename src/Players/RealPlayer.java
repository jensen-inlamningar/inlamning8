package Players;

import java.util.Scanner;

public class RealPlayer extends Player {


    //konstruktor som matchar parentklassen
    public RealPlayer(String name, int wins) {
        super(name, wins);
    }


    //Användarens metod för hur valet blir
    public Move tactic() {

        System.out.println("Choose your move: \n" + "1: Sciccors\n" + "2: Paper\n" + "3: Rock");
        Scanner scanner = new Scanner(System.in);

        while (!scanner.hasNextInt()){
            scanner.nextLine();
            System.out.println("Please input a number.");
        }
        int input = scanner.nextInt();
        return switch (input) {
            case 1 -> Move.SCISSORS;
            case 2 -> Move.PAPER;
            default -> Move.ROCK;
        };


    }

    //Den här metoden adderar till vinsterna som spelarna har
    public void incrementWins() {
        wins = wins + 1;
    }


}
