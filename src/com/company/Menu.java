package com.company;

import com.company.Game;

import java.util.Scanner;

public class Menu {

    //En vanlig meny som visar vilka val man kan gör när man startar programmet
    public void gameMenu() {
        Game game = new Game();
        Scanner scanner = new Scanner(System.in);
        boolean menuOn = true;
        while (menuOn) {
            System.out.println("Welcome to Rock, Paper & Scissors!");
            System.out.println("");
            System.out.println("1: Play\n" + "2: View stats\n" + "3: End program");
            int choice = scanner.nextInt();
            switch (choice) {
                case 1 -> game.gameON();
                case 2 -> game.printGameStats();
                case 3 -> menuOn = false;
            }

        }
    }
}
