package com.company;

import Players.Computer;
import Players.RealPlayer;

public class Game {

    //Här skapar jag spelarna
    private final RealPlayer realPlayer = new RealPlayer("Player", 0);
    private final Computer computer = new Computer("Computer", 0);


    //Metod för att sätta igång spelet
    public void gameON() {


        //Whileloopen strular då valen inte tas in ordentligt,
        //Har försökt felsöka och här har jag använt mig av sout för
        //kunna se om spelarna får sina val som dom ska.
        //Spelarna får sina val första rundan men sedan startas valen om av
        //någon anledning, sedan fungerar inte spelet som det ska och jag
        //har försökt felsöka genom att debugga men vet inte vad jag har gjort för fel.
        System.out.println(realPlayer.tactic().toString());
        System.out.println(computer.tactic().toString());
        do  {
            System.out.println(realPlayer.tactic().toString() + "in while loop");
            System.out.println(computer.tactic().toString() + "in while loop");
            if (realPlayer.tactic() == (computer.tactic())) {
                System.out.println("It's a tie\n The match will continue");
            } else if (computer.tactic().beats(realPlayer.tactic())) {
                System.out.println("Computer wins! Stupid...");
                computer.incrementWins();
                break;
            } else {
                System.out.println("YOU WIN!!");
                realPlayer.incrementWins();
                break;
            }

        }while ((realPlayer.tactic() != null ));
    }

    //Här är en metod där jag helt enkelt skriver ut antalet vinster hos spelarna
    public void printGameStats() {
        System.out.println("Player wins :" + realPlayer.getWins());
        System.out.println("Computer Wins :" + computer.getWins());
    }

}
